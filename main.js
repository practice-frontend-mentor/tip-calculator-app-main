/*Obtener elementos*/
const bill=document.getElementById('Bill');
const five=document.getElementById('five');
const ten=document.getElementById('ten');
const fiveteen=document.getElementById('fiveteen');
const twentyfive=document.getElementById('twenty-five');
const fifty=document.getElementById('fifty');
const custom=document.getElementById('custom');
const numberP=document.getElementById('numberP');
const reset=document.getElementById('reset');

const tipAmount=document.getElementById('tip-amount');
const total=document.getElementById('total');

const error=document.querySelector('.error');



function calc(tp,prs){
    let billtotal=parseFloat(bill.value);
    let tip=parseFloat(tp);
    if(prs==null||prs==''||prs<1){
        error.style.display='block';
    }else{
        
        let persons=parseInt(prs);
        error.style.display='none';
        
        let totalTip=(billtotal*tip)/100;
        let totalTipPerson=totalTip/persons;
        tipAmount.innerText=totalTipPerson;

        let totalPerson=((billtotal+totalTip)/persons);
        total.innerHTML=totalPerson;
    }
}

function cls(){
    bill.value='';
    custom.value='';
    numberP.value='';
    tipAmount.innerHTML='0.00';
    total.innerHTML='0.00';
}


/*Botones*/
five.addEventListener('click',()=>{
    calc(5,numberP.value);
})

ten.addEventListener('click',()=>{
    calc(10,numberP.value);
})

fiveteen.addEventListener('click',()=>{
    calc(15,numberP.value);
})

twentyfive.addEventListener('click',()=>{
    calc(25,numberP.value);
});

fifty.addEventListener('click',()=>{
    calc(50,numberP.value);
});


custom.addEventListener('click',()=>{
    calc(custom.value,numberP.value);
});

custom.addEventListener('keyup',()=>{
    calc(custom.value,numberP.value);
});

reset.addEventListener('click',()=>{
    cls();
});